#pragma once
#include<malloc.h>
#include<unistd.h>
#include<stdint.h>
#include<list>
#include<queue>
#include<vector>
#include<string>
#include<unordered_map>
#include<map>
#include <functional>
using namespace std;
typedef vector<uint8_t> nums;
namespace std {
    template <> //function-template-specialization
        class hash<nums>{
        public :
            size_t operator()(const nums &name ) const
            {
                size_t re = name.size() > 0 ? hash<uint8_t>()(name[0]):0;
                for (int i = 1; i < name.size(); i++) {
                    re = re ^ hash<uint8_t>()(name[i]);
                }
                return re;
            }
    };
};

//第一次select只是查询，第二次才是加入LRU
//
//第一个队列是FIFO，第二个队列是LRU-2（访问第二次将加入第二个队列）
class Lru_2{
    list<nums> fifo;
    unordered_map<nums, nums> fifo_map;
    int fifo_capacity;
    //
    list<nums> lru;
    unordered_map<nums, nums> lru_map;
    int lru_capacity;


    public:
    Lru_2(int fifo_c,int lru_c);
    nums select(nums key);
    void put(nums key, nums value);
};
