#pragma once
#include<iostream>
#include <stdio.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <string.h>
#include <cstring>
#include <netinet/in.h>
#include <ctype.h>
#include <iostream>
#include <netdb.h>
#include <ifaddrs.h>
#include <malloc.h>
#include <errno.h>
#include <stdint.h>
#include <deque>
#include <list>
#include "Lru.h"
#include "RateLimiter.h"
#include<unordered_map>
#include<vector>
#include<algorithm>
using namespace std;
class name {
    public:
    uint8_t buf[10240];
    uint8_t sendbuf[10240];
    int socketfd;
    int mainSocketfd;
    struct sockaddr_in clientAddress;
    struct sockaddr_in serverAddress;
    struct sockaddr_in localAddress;
    socklen_t len;
    int count;
    Lru_2 *lru;
    RateLimiter *r_ptr;

    public:
    name(int off, int dns_port);
    void process();

    private:
    vector<uint8_t> domain;//存放域名
    int query();
    char * ip;
    private:
    void parse(uint8_t * buf);

};
