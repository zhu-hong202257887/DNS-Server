# DNS

#### 介绍
高性能、具有一定并发量、稳定的DNS服务器，支持几乎所有类型的DNS查询，因为是从根域名服务器开始查询，所以保证了信息的准确。
技术栈：LRU、令牌桶算法、shell脚本测试、线程池、智能指针等技术

#### 开发工具
操作系统：centOS7、Ubuntu18.04等常用Linux系统
编译器：g++和shell
版本控制：git
调试工具：gdb、dig、


#### 安装教程
git clone https://gitee.com/zhu-hong202257887/DNS-Server
cd DNS
g++ *.cpp -o main -pthread

#### 使用说明

1打开服务器           :./main 端口号
2测试一条dns查询      :dig @127.0.0.1 -p 8888 baidu.com A
3使用shell脚本就行测试 :./test.sh

#### 技术要点

1.  非阻塞的UDP/IO
2.  并发模型Reactor
3.  充分利用多核CPU的性能，以多线程的形式实现服务器，实现线程池避免线程频繁创建和销毁造成的系统开销。
4.  线程同步避免死锁
5.  LRU-2实现的高速缓冲
6.  令牌桶算法防止网络拥塞和突然的爆发流量。
7.  shell脚本就行测试


#### 文档
|  DNS知识和工具  | 项目架构 | 核心类图 | 两个重要的算法 |
|---------------|---------|---------|-------------|
|[DNS过程](https://gitee.com/zhu-hong202257887/local-dns-server/blob/master/doc/%E5%88%A9%E7%94%A8nslookup%E6%8E%A2%E7%B4%A2DNS%E7%9A%84%E8%BF%87%E7%A8%8B.md) / [DNS报文](https://gitee.com/zhu-hong202257887/local-dns-server/blob/master/doc/%E5%AE%9E%E9%99%85DNS%E6%8A%A5%E6%96%87%E8%A7%A3%E6%9E%90.md)|         | [重要class](https://mmm2r23z52.feishu.cn/docx/doxcnKgsVMfepo5nipMnSbnMidf?from=from_copylink)        |[LRU-2](https://mmm2r23z52.feishu.cn/docx/doxcnrZ8F12S9P4EMLAYbDJdoPe) / [令牌桶算法](https://mmm2r23z52.feishu.cn/docx/doxcnGrctQy3Nbz1yrzNbObdPlc)|




