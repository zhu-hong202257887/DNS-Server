#include "Lru.h"
Lru_2::Lru_2( int fifo_c, int lru_c) {
    fifo_capacity = fifo_c;
    lru_capacity = lru_c;
}
//查询
nums Lru_2::select(nums key)  {
    nums ret;
    if (lru_map.find(key) != lru_map.end()) {//把查询到的提前
        lru.remove(key);
        lru.push_front(key);
        return lru_map[key];
    } else if (fifo_map.find(key) != fifo_map.end()) {//先增加再删除
        ret = fifo_map[key];//增加
        lru.push_front( key );
        lru_map[key] = fifo_map[key];
        if (lru.size() > lru_capacity) {//如果超过极限就删除
            nums temp = lru.back();
            lru.pop_back();
            lru_map.erase(temp);
        }
        fifo.remove( key );
        fifo_map.erase(key);
        return ret;
    }
    return ret;
}
//增加
void Lru_2::put(nums key, nums value) {
    if(lru_map.find(key) != lru_map.end() ) {//在列中，并且已经被查询过
        return ;
    }
    //此时不在fifo和lru中，加入fifo
    fifo.push_front(key);
    fifo_map[key] = value;
    if (fifo.size() > fifo_capacity) {
        fifo.pop_back();
        fifo_map.erase(key);
    }    
}