#pragma once
 
#include <mutex>
 
#define RETRY_IMEDIATELY_TIMES 30
 
class RateLimiter {
public:
    RateLimiter(int qps);
 
    void pass();
 
private:
    //互斥锁
    std::mutex mtx;
 
    //获得当前时间, 单位ns
    long now();
 
    //更新令牌桶中的令牌
    void supplyTokens();
 
    //尝试获得令牌
    bool tryGetToken();
 
    //必定成功获得令牌
    void mustGetToken();
 
    //令牌桶大小
    int bucketSize;
 
    //剩下的令牌数
    int tokenLeft;
 
    //补充令牌的单位时间
    long supplyUnitTime;
 
    //上次补充令牌的时间
    long lastAddTokenTime;
};
 